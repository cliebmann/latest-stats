### Annoyances
I'm keeping the environment variables / configuration in a separate repo
to keep this repo clear of details related to my development environment and
account details (Twilio). It's kind of annoying and really is just an
exercise in building a [Twelve-Factor App](https://12factor.net).

## Runbook

To run the examples you can either source the environment in your shell
or create a copy of run.sh and hardcode the local path.

```shell
# Use virtualenv or whatever python installation
$ source venv/bin/activate
# Run in `latest-stats`
$ source /path/to/local/env/file
```

### Command Line Interface

```shell
# Run end-to-end
$ cat samples/sample_account.txt | /path/to/local/run_v1.sh > /dev/null
$ cat samples/sample_account.txt | /path/to/local/run_v2.sh > /dev/null
```

#### v1
Get roster from yahoo api
Get stats from basketball-referene.com
(Downside: stats are not live)

```shell
$ cat samples/sample_account.txt
{"account_id": "yahoo-quickstart", "team_key": "375.l.55116.t.8"}
```

```shell
$ cat samples/sample_account.txt | get_roster.py 2> /dev/null | head -n 2
{"name": "James Harden", "team": "Houston Rockets"}
{"name": "Jonathon Simmons", "team": "Orlando Magic"}
```

```shell
$ cat samples/sample_player_names.txt | find_player_id.py 2> /dev/null
{"name": "Jonathon Simmons", "team": "Orlando Magic", "player_id": "simmojo02"}
{"name": "James Harden", "team": "Houston Rockets", "player_id": "hardeja01"}
```

```shell
$ cat samples/sample_player_id.txt | get_latest_stats.py 2> /dev/null
{"name": "Jonathon Simmons", "team": "Orlando Magic", "player_id": "simmojo02", "mp": "27:32", "fg": 5, "fga": 10, "fg3": 0, "ft": 2, "fta": 2, "trb": 2, "ast": 0, "stl": 0, "blk": 0, "tov": 1, "pts": 12}
{"name": "James Harden", "team": "Houston Rockets", "player_id": "hardeja01", "mp": "36:24", "fg": 10, "fga": 23, "fg3": 4, "ft": 3, "fta": 4, "trb": 6, "ast": 11, "stl": 1, "blk": 0, "tov": 3, "pts": 27}
```

```shell
$ cat samples/sample_player_game.txt | send_twilio_sms.py 2> /dev/null
{"name": "Jonathon Simmons", "team": "Orlando Magic", "player_id": "simmojo02", "mp": "15:08", "fg": 1, "fga": 3, "fg3": 0, "ft": 1, "fta": 2, "trb": 1, "ast": 1, "stl": 1, "blk": 0, "tov": 0, "pts": 3, "twilio-msg-sid": "SMa8f1cfe028da4a648623ad40ff2bbe05"}
{"name": "James Harden", "team": "Houston Rockets", "player_id": "hardeja01", "mp": "40:47", "fg": 10, "fga": 20, "fg3": 3, "ft": 17, "fta": 21, "trb": 2, "ast": 11, "stl": 2, "blk": 2, "tov": 1, "pts": 40, "twilio-msg-sid": "SM497e6d2f12a04ccbab9aff8709aa9c89"}
```

```shell
$ time cat samples/sample_account.txt | /path/to/local/run_and_save_v1.sh > /dev/null

real	0m58.087s
user	0m8.764s
sys	0m0.502s
$ rundir=$(ls -td runs/v1/*)
$ tree $rundir
runs/v1/20180107_183806
├── account.jsn
├── latest_stats.jsn
├── log.txt
├── player_ids.jsn
├── roster.jsn
└── twilio_sids.jsn

0 directories, 6 files
$ grep "latest-stats" $rundir/run_and_save_v1.log | grep "INFO\|WARNING\|ERROR"
2018-01-07 18:38:06,851 - latest-stats.send_twilio_sms - INFO  - START
2018-01-07 18:38:07,031 - latest-stats.find_player_id - INFO  - START
2018-01-07 18:38:07,034 - latest-stats.get_roster - INFO  - START
2018-01-07 18:38:07,036 - latest-stats.get_latest_stats - INFO  - START
2018-01-07 18:38:11,559 - latest-stats.get_roster - INFO  - STOP
2018-01-07 18:38:43,003 - latest-stats.find_player_id - INFO  - STOP
2018-01-07 18:38:44,475 - latest-stats.get_latest_stats - WARNING  - Did not find latest stats for player[{'name': 'Zach LaVine', 'team': 'Chicago Bulls', 'player_id': 'lavinza01'}]
2018-01-07 18:38:58,616 - latest-stats.get_latest_stats - WARNING  - Did not find latest stats for player[{'name': 'Jeff Teague', 'team': 'Minnesota Timberwolves', 'player_id': 'teaguje01'}]
2018-01-07 18:39:00,811 - latest-stats.get_latest_stats - INFO  - STOP
2018-01-07 18:39:04,036 - latest-stats.send_twilio_sms - INFO  - STOP
```

#### v2
Get roster and stats from yahoo api

```shell
$ yahoo_raw_roster.py | python -m json.tool > raw_roster.jsn
```

```shell
# takes optional date string "YYYY-MM-DD" as first parameter
$ cat samples/sample_account.txt | yahoo_latest_stats.py 2> /dev/null | head -n 2
{"name": "Jeff Teague", "team": "Minnesota Timberwolves", "player_key": "375.p.4624", "date": "2018-01-12", "stats": {"fgm/a": "3/11", "ftm/a": "5/6", "3ptm": "1", "pts": "12", "reb": "4", "ast": "8", "stl": "4", "blk": "0", "to": "3"}}
{"name": "Jonathon Simmons", "team": "Orlando Magic", "player_key": "375.p.5526", "date": "2018-01-12", "stats": {"fgm/a": "8/11", "ftm/a": "6/6", "3ptm": "1", "pts": "23", "reb": "0", "ast": "5", "stl": "0", "blk": "0", "to": "2"}}
```

```shell
$ cat samples/sample_yahoo_player_stats.txt | yahoo_extract_fields.py 2> /dev/null
{"name": "Jonathon Simmons", "team": "Orlando Magic", "date": "2018-01-12", "fgm/a": "8/11", "ftm/a": "6/6", "3ptm": "1", "pts": "23", "reb": "0", "ast": "5", "stl": "0", "blk": "0", "tov": "2"}
{"name": "Thaddeus Young", "team": "Indiana Pacers", "date": "2018-01-12", "fgm/a": "5/16", "ftm/a": "0/0", "3ptm": "2", "pts": "12", "reb": "7", "ast": "2", "stl": "1", "blk": "0", "tov": "2"}
```

```shell
$ time cat samples/sample_account.txt | run_and_save_v2.sh > /dev/null

real	0m4.679s
user	0m0.979s
sys	0m0.219s
$ rundir=$(ls -td runs/v2/*)
$ tree $rundir
runs/v2/20180113_131807
├── account.jsn
├── extract_fields.jsn
├── latest_stats.jsn
├── run_and_save_v2.log
└── twilio_sids.jsn

0 directories, 5 files
$ grep "latest-stats" $rundir/run_and_save_v2.log | grep "INFO\|WARNING\|ERROR"
2018-01-13 13:18:07,476 - latest-stats.yahoo_extract_fields - INFO  - START
2018-01-13 13:18:07,736 - latest-stats.yahoo_latest_stats - INFO  - START
2018-01-13 13:18:07,844 - latest-stats.send_twilio_sms - INFO  - START
2018-01-13 13:18:08,696 - latest-stats.yahoo_latest_stats - INFO  - STOP
2018-01-13 13:18:08,728 - latest-stats.yahoo_extract_fields - INFO  - STOP
2018-01-13 13:18:11,735 - latest-stats.send_twilio_sms - INFO  - STOP
```

#### Deduper

```shell
$ cat samples/sample_dedupe.txt | dedupe.py 2> /dev/null
{"name": "Jonathon Simmons", "team": "Orlando Magic", "player_key": "375.p.5526", "date": "2018-01-12", "stats": {"fgm/a": "8/11", "fgpct": ".727", "ftm/a": "6/6", "ftpct": "1.000", "3ptm": "1", "pts": "23", "reb": "0", "ast": "5", "stl": "0", "blk": "0", "tov": "2"}}
{"name": "Thaddeus Young", "team": "Indiana Pacers", "player_key": "375.p.4290", "date": "2018-01-12", "stats": {"fgm/a": "5/16", "fgpct": ".313", "ftm/a": "0/0", "ftpct": "-", "3ptm": "2", "pts": "12", "reb": "7", "ast": "2", "stl": "1", "blk": "0", "tov": "2"}}
```

```shell
$ cmd='cat samples/sample_account.txt | yahoo_latest_stats.py "2018-01-12" 2> /dev/null'
$ eval $cmd | wc -l
       8
$ cat <(eval $cmd) <(eval $cmd) | wc -l
      16
$ cat <(eval $cmd) <(eval $cmd) | dedupe.py 2> /dev/null | wc -l
       8
```

#### Repeated Timer

```shell
$ repeated_timer.py 1 cat samples/sample_account.txt
2018-01-13 18:27:37,271 - latest-stats.repeated_timer - INFO  - START
2018-01-13 18:27:37,271 - latest-stats.repeated_timer - DEBUG  - Running command 'cat samples/sample_account.txt'
{"account_id": "yahoo-quickstart", "team_key": "375.l.55116.t.8"}
2018-01-13 18:27:37,283 - latest-stats.repeated_timer - DEBUG  - Executed command
{"account_id": "yahoo-quickstart", "team_key": "375.l.55116.t.8"}
2018-01-13 18:27:47,308 - latest-stats.repeated_timer - DEBUG  - Executed command
^CTraceback (most recent call last):
  File "/path/to/latest-stats/repeated_timer.py", line 34, in <module>
    time.sleep(delay)
KeyboardInterrupt
```

```shell
$ { repeated_timer.py 0.1 cat samples/sample_account.txt | yahoo_latest_stats.py | dedupe.py | yahoo_extract_fields.py | send_twilio_sms.py ; } 2> logs/repeated_timer.log
{"name": "Milos Teodosic", "team": "Los Angeles Clippers", "date": "2018-01-13", "fgm/a": "4/6", "ftm/a": "0/0", "3ptm": "3", "pts": "11", "reb": "1", "ast": "5", "stl": "0", "blk": "0", "tov": "1", "twilio-msg-sid": "SM5f8ee7fe6b4549ef918dab580398eea5"}
{"name": "Jahlil Okafor", "team": "Brooklyn Nets", "date": "2018-01-13", "fgm/a": "1/2", "ftm/a": "0/0", "3ptm": "0", "pts": "2", "reb": "0", "ast": "1", "stl": "0", "blk": "0", "tov": "1", "twilio-msg-sid": "SM55c92b4fb0774a9f9ed2a936647ff5ec"}
{"name": "Willie Cauley-Stein", "team": "Sacramento Kings", "date": "2018-01-13", "fgm/a": "10/16", "ftm/a": "2/4", "3ptm": "1", "pts": "23", "reb": "13", "ast": "4", "stl": "1", "blk": "1", "tov": "2", "twilio-msg-sid": "SM21b3b54d1c64425ea78ca0304a01a48a"}
{"name": "Zach LaVine", "team": "Chicago Bulls", "date": "2018-01-13", "fgm/a": "2/2", "ftm/a": "0/0", "3ptm": "1", "pts": "5", "reb": "0", "ast": "1", "stl": "0", "blk": "0", "tov": "0", "twilio-msg-sid": "SM0e3d67e955484acbb223b1c32b503694"}
^C$
$ grep "latest-stats" logs/repeated_timer.log | grep "INFO\|WARNING\|ERROR"
2018-01-13 20:21:08,287 - latest-stats.repeated_timer - INFO  - START
2018-01-13 20:21:08,455 - latest-stats.yahoo_extract_fields - INFO  - START
2018-01-13 20:21:08,538 - latest-stats.dedupe - INFO  - START
2018-01-13 20:21:08,860 - latest-stats.send_twilio_sms - INFO  - START
2018-01-13 20:21:08,996 - latest-stats.yahoo_latest_stats - INFO  - START
$ grep "yahoo_latest_stats" logs/repeated_timer.log | grep "IN account" | wc -l
      13
$ grep "yahoo_latest_stats" logs/repeated_timer.log | grep "OUT player" | wc -l
      52
$ grep "dedupe" logs/repeated_timer.log | grep "IN data" | wc -l
      52
$ grep "dedupe" logs/repeated_timer.log | grep "OUT data" | wc -l
       4
$ grep "send_twilio_sms" logs/repeated_timer.log | grep "IN data" | wc -l
       4
$ grep "send_twilio_sms" logs/repeated_timer.log | grep "OUT data" | wc -l
       4
```

### Redis Deduper

```shell
$ redis-server
... PORT = 6379 ...
```

```shell
$ echo $REDIS_URL
redis://localhost:6379
$ cat samples/sample_dedupe.txt | dedupe.py --redis
2018-01-21 19:07:15,504 - latest-stats.dedupe - INFO  - START (with <class '__main__.RedisFilter'>)
...
```

```shell
$ redis-cli
127.0.0.1:6379> zrange deduper 0 -1
1) "9f5d108242495b114e31e08153e5fa737ab821bd"
2) "f3288938f5134fe2c328478267953d5671d5b595"
```

### Flask Server

```shell
$ flask_app.py
2018-01-21 19:17:27,461 - latest-stats.dedupe - INFO  - START (with <class 'dedupe.RedisFilter'>)
2018-01-21 19:17:27,461 - latest-stats.yahoo_extract_fields - INFO  - START
2018-01-21 19:17:28,016 - werkzeug - INFO  -  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
...
2018-01-21 19:18:26,003 - latest-stats.yahoo_latest_stats - INFO  - START
2018-01-21 19:18:26,003 - latest-stats.yahoo_latest_stats - DEBUG  - IN account[{'account_id': 'yahoo-quickstart', 'team_key': '375.l.55116.t.8'}]
...
```

```shell
$ curl http://0.0.0.0:5000/get
{
  "fields": [
    {
      "3ptm": "0",
      "ast": "0",
      "blk": "0",
      "date": "2018-01-21",
      "fgm/a": "0/2",
      "ftm/a": "0/0",
      "name": "Thaddeus Young",
      "pts": "0",
      "reb": "1",
      "stl": "1",
      "team": "Indiana Pacers",
      "tov": "0"
    },
    {
      "3ptm": "0",
      "ast": "4",
      "blk": "2",
      "date": "2018-01-21",
      "fgm/a": "4/7",
      "ftm/a": "1/1",
      "name": "Jonathon Simmons",
      "pts": "9",
      "reb": "2",
      "stl": "1",
      "team": "Orlando Magic",
      "tov": "3"
    }
  ],
  "timing": 0.04869599999999985
}
$ curl http://0.0.0.0:5000/get/team/375.l.55116.t.8
[]
$ curl http://0.0.0.0:5000/get/team/375.l.55116.t.8/date/2018-01-20
[
  {
    "3ptm": "0",
    "ast": "10",
    "blk": "0",
    "date": "2018-01-20",
    "fgm/a": "2/11",
    "ftm/a": "1/2",
    "name": "Jeff Teague",
    "pts": "5",
    "reb": "3",
    "stl": "0",
    "team": "Minnesota Timberwolves",
    "tov": "2"
  },
...
]
$ curl http://0.0.0.0:5000/get/team/375.l.55116.t.8/date/2018-01-20
[]
```

```shell
$ time curl -w %{time_total} http://0.0.0.0:5000/get/
{
  ...
  "timing": 0.09477099999999994
}
2.147
real	0m2.166s
user	0m0.006s
sys	0m0.009s

$ time curl -w %{time_total} http://0.0.0.0:5000/get/
{
  ...
"timing": 0.04869599999999985
}
0.334
real	0m0.346s
user	0m0.006s
sys	0m0.006s

$ time curl -w %{time_total} http://0.0.0.0:5000/send/
{
  "timing": 0.10891399999999996
}
2.002
real	0m2.021s
user	0m0.006s
sys	0m0.008s
```

#### InsecureTransportError
CLIOAuthHandshakePolicy does not support https. Somehow http worked previously but ran into error:
```shell
oauthlib.oauth2.rfc6749.errors.InsecureTransportError: (insecure_transport) OAuth 2 MUST utilize https.
```

For a quick fix set an env var directly from `get_refresh_token`:
```python
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
```

#### Multiple processes
```shell
$ lsof -i -P | grep 8080
python3.6 6187 craig    4u  IPv4 0x5bfa1c5c05d4d3ff      0t0  TCP *:8080 (LISTEN)
python3.6 6188 craig    4u  IPv4 0x5bfa1c5c05d4d3ff      0t0  TCP *:8080 (LISTEN)
python3.6 6188 craig    5u  IPv4 0x5bfa1c5c05d4d3ff      0t0  TCP *:8080 (LISTEN)
$ ps 6188
  PID   TT  STAT      TIME COMMAND
 6188 s000  S+     0:03.95 /Users/craig/Workspace/fantasy-17-18/venv/bin/python /Users/craig/Workspace/fantasy-17-18/latest-stats/flask_app.py
$ ps 6187
  PID   TT  STAT      TIME COMMAND
 6187 s000  S+     0:01.04 python /Users/craig/Workspace/fantasy-17-18/latest-stats/flask_app.py
$ kill 6187
$ kill 6188
-bash: kill: (6188) - No such process
$ lsof -i -P | grep 8080
$
```

#### pfctl interference
pfctl config mapping traffic from port 80 to 8080 interfered with curl traffic. Disabled the filter.

Use `sudo` with `-E` option to forward environment variables:
```shell
$ sudo -E flask_app.py
```

#### Heroku
Using python 3.6.4 to satisfy heroku:
```shell
$ which python3
/usr/local/bin/python3
$ python3 --version
Python 3.6.4
$ virtualenv --python=/usr/local/bin/python3 venv
...
```
