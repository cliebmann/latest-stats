#!/usr/bin/env python

from interact import interact, InteractContinue
from log_utils import logger_name

import redis

import hashlib
import logging
import json
import time
import sys
import os

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
module_logger = logging.getLogger(logger_name(__file__))

class ElementExists(KeyError): pass
class FilterInitError(Exception): pass
class RedisUrlMissing(FilterInitError, KeyError): pass

def digester(data):
    return hashlib.sha1(json.dumps(data, sort_keys=True).encode('utf-8')).hexdigest()

class LocalFilter(object):
    def __init__(self):
        self.set = set()

    def add(self, data):
        digest = digester(data)

        if digest in self.set:
            raise ElementExists()

        self.set.add(digest)

    def clear(self):
        self.set.clear()

class RedisFilter(object):
    def __init__(self):
        try:
            url = os.environ['REDIS_URL']
            self.redis = redis.from_url(url)
            self.my_key = 'deduper'
        except KeyError as e:
            module_logger.warn(f'{type(e)}: {e}')
            raise RedisUrlMissing()

    def add(self, data):
        digest = digester(data)
        now = int(time.time())

        # arguments follow strict order even though `type(self.redis) == <class 'redis.client.Redis'>`
        # and not 'redis.StrictRedis'
        if self.redis.zadd(self.my_key, digest, now) == 0:
            raise ElementExists()

    def clear(self):
        self.redis.delete(self.my_key)

def make_filter():
    try:
        return RedisFilter()
    except KeyError:
        return LocalFilter()

def raises_or_none(exception):
    if exception:
        raise exception
    else:
        return None

class Deduper(object):

    def __init__(self, FilterPolicy, dupe_error=None):
        module_logger.info(f'START (with {FilterPolicy})')

        self.filter = FilterPolicy()
        self.dupe_error = dupe_error

    @classmethod
    def WithFallback(cls, FilterPolicy, FallbackPolicy, **kwargs):
        try:
            return cls(FilterPolicy, **kwargs)
        except FilterInitError:
            module_logger.warn(f'Failed to init Deduper (with {FilterPolicy})')
            return cls(FallbackPolicy, **kwargs)

    def __del__(self):
        module_logger.info('STOP')

    def __call__(self, data):
        module_logger.debug(f'IN data[{data}]')

        try:
            self.filter.add(json.dumps(data))
            module_logger.debug(f'OUT data[{data}]')
            return data
        except ElementExists:
            raises_or_none(self.dupe_error)

    def clear(self):
        self.filter.clear()

if __name__ == '__main__':
    policy = RedisFilter if len(sys.argv) == 2 and sys.argv[1] == '--redis' else LocalFilter

    if policy == LocalFilter:
        deduper = Deduper(policy, dupe_error=InteractContinue)
    else:
        deduper = Deduper.WithFallback(policy, LocalFilter, dupe_error=InteractContinue)

    interact(deduper)
