#!/usr/bin/env python

from log_utils import logger_name

from bs4 import BeautifulSoup
import requests
import logging
import json
import re
import sys

# returns player_id on success, None on failure
def player_id_from_redirect(url):
    m = re.match('https://www.basketball-reference.com/players/[a-z]/(?P<player_id>\w+).html', url)
    if m:
        return m['player_id']

    return None

def player_id_from_search_results(page, team):
    bs = BeautifulSoup(page.text, 'html.parser')
    players = bs.find('div', {'id': 'players'}).findAll('div', {'class': 'search-item'})
    for player in players:
        player_team = player.find('div', {'class': 'search-item-team'}).text
        if team in player_team:
            player_id = player.find('div', {'class': 'search-item-url'}).text.split('/')[-1].split('.')[0]
            return player_id

    return None

def find_player_id(name, team):
    search_url = 'https://www.basketball-reference.com/search/search.fcgi'
    page = requests.get(search_url, {'search': name})

    player_id = player_id_from_redirect(page.url)
    if player_id:
        return player_id

    player_id = player_id_from_search_results(page, team)
    if player_id:
        return player_id

    return None

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
    logger = logging.getLogger(logger_name(__file__))

    logger.info('START')

    for line in sys.stdin:
        player = json.loads(line)
        logger.debug(f'IN player[{player}]')

        try:
            player_id = find_player_id(player['name'], player['team'])
            if player_id:
                player['player_id'] = player_id

                print(json.dumps(player), flush=True)
                logger.debug(f'OUT player[{player}]')

            else:
                logger.warn(f'Did not find player_id for player[{player}]')

        except AttributeError:
            logger.warn(f'Did not find player_id for player[{player}]')

    logger.info('STOP')
