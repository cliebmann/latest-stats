#!/usr/bin/env python

from yahoo_latest_stats import YahooLatestStatsGateway
from yahoo_extract_fields import FieldExtractor
from dedupe import Deduper, RedisFilter, LocalFilter
from send_twilio_sms import SMSMessager

from oauth_tools.token_gateway import LocalTokenGateway
from oauth_tools.handshake_policy import FlaskOAuthHandshakePolicy

from log_utils import logger_name

from flask import Flask
from flask import jsonify
from flask import request, redirect

import datetime
import logging
import json
import time
import os

CONSUMER_KEY = os.environ['YAHOO_CONSUMER_KEY']
CONSUMER_SECRET = os.environ['YAHOO_CONSUMER_SECRET']

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

app = Flask(__name__)

logger = logging.getLogger(logger_name(__file__))

# deduper = Deduper.WithFallback(RedisFilter, LocalFilter)
deduper = Deduper(LocalFilter)

MyTokenGateway = LocalTokenGateway
MyOAuthHandshakePolicy = FlaskOAuthHandshakePolicy
MyYahooLatestStatsGateway = YahooLatestStatsGateway.WithOAuthClient

field_extractor = FieldExtractor()

messager = SMSMessager()

oauth_callback_state = None

def sample_account(team_key=None):
    return {"account_id": "yahoo-quickstart", "team_key": team_key}

def paragraph(text):
    return f'<p>{text}</p>'

def anchor(url):
    return f'<a>{url}</a>'

def anchor_if_authorized(url, authorized):
    if authorized:
        return anchor(url)
    else:
        return f'{url} (need to authorize)'

@app.route('/')
def index():
    token_gateway = MyTokenGateway(sample_account()['account_id'])
    token = token_gateway.get()
    authorized = False if MyOAuthHandshakePolicy.needs_refresh_token(token) else True

    outs = []

    yesterday = datetime.datetime.today() - datetime.timedelta(days=1)

    outs.append('<html><body>')
    outs.append('<p>Hello, World!</p>')
    outs.append(paragraph(anchor_if_authorized(f'{request.url_root}get', authorized)))
    outs.append(paragraph(anchor_if_authorized(f'{request.url_root}get/team/375.l.55116.t.8/date/{yesterday.strftime("%Y-%m-%d")}', authorized)))
    outs.append(paragraph(anchor(f'{request.url_root}authorize')))
    outs.append('</body></html>')

    return '\n'.join(outs)

@app.route('/authorize')
def authorize():
    token_gateway = MyTokenGateway(sample_account()['account_id'])
    token = token_gateway.get()
    logger.debug(token)

    if MyOAuthHandshakePolicy.needs_refresh_token(token):
        auth_url, state = MyOAuthHandshakePolicy.get_auth_url(CONSUMER_KEY)

        global oauth_callback_state
        oauth_callback_state = state
        logger.debug(auth_url, state)
        return redirect(auth_url)

    else:
        return 'Already authorized'

@app.route('/oauth_callback')
def oauth_callback():
    token_gateway = MyTokenGateway(sample_account()['account_id'])

    callback_path = request.full_path
    token = MyOAuthHandshakePolicy.get_refresh_token(CONSUMER_KEY, CONSUMER_SECRET, callback_path)
    if token:
        token_gateway.save(token)
        return redirect('/')
    else:
        return f'Error getting refresh token\n'

# team format is '<game>.l.<league>.t.<team>''
# date format is 'YYYY-MM-DD'
@app.route('/get/')
@app.route('/get/team/<team>')
@app.route('/get/team/<team>/date/<date>')
def get(team='375.l.55116.t.8', date=None):
    begin = time.process_time()

    token_gateway = MyTokenGateway(sample_account()['account_id'])
    token = token_gateway.get()

    if MyOAuthHandshakePolicy.needs_refresh_token(token):
        return redirect('/authorize')

    oauth_client = MyOAuthHandshakePolicy.make_client(CONSUMER_KEY, CONSUMER_SECRET, token, token_gateway)

    stats_gtwy = MyYahooLatestStatsGateway(oauth_client, date)
    stats = filter(None, (deduper(s) for s in stats_gtwy(sample_account(team))))
    fields = (field_extractor(s) for s in stats)
    real_fields = list(fields)

    end = time.process_time()

    return jsonify({'fields': real_fields, 'timing': end-begin})

# team format is '<game>.l.<league>.t.<team>''
# date format is 'YYYY-MM-DD'
@app.route('/send/')
@app.route('/send/team/<team>')
@app.route('/send/team/<team>/date/<date>')
def send(team='375.l.55116.t.8', date=None):
    begin = time.process_time()

    token_gateway = MyTokenGateway(sample_account()['account_id'])
    token = token_gateway.get()

    if MyOAuthHandshakePolicy.needs_refresh_token(token):
        return redirect('/authorize')

    oauth_client = MyOAuthHandshakePolicy.make_client(CONSUMER_KEY, CONSUMER_SECRET, token, token_gateway)

    stats_gtwy = MyYahooLatestStatsGateway(oauth_client, date)
    stats = filter(None, (deduper(s) for s in stats_gtwy(sample_account(team))))
    fields = (field_extractor(s) for s in stats)
    msgs = (messager(f) for f in fields)
    real_msgs = list(msgs)

    end = time.process_time()

    return jsonify({'timing': end-begin})

@app.route('/clear')
def clear():
    deduper.clear()

if __name__ == '__main__':
    # app.run(port=80, debug=True)
    app.run(port=int(os.environ.get('PORT', 5000)), debug=False)
