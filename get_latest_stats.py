#!/usr/bin/env python

from interact import interact, InteractContinue
from log_utils import logger_name

from bs4 import BeautifulSoup
import requests
import logging
import json
import sys

# hardcoded year
def player_url(player_id):
    url_prefix = 'https://www.basketball-reference.com/players'
    url_suffix = 'gamelog/2018'
    return f'{url_prefix}/{player_id[0]}/{player_id}/{url_suffix}'

def extract_stats(page):
    target_fields = [('mp', str), ('fg', int), ('fga', int), ('fg3', int),
      ('ft', int), ('fta', int), ('trb', int), ('ast', int), ('stl', int),
      ('blk', int), ('tov', int), ('pts', int)]

    bs = BeautifulSoup(page.text, 'html.parser')
    latest_row = bs.find('table', {'id': 'pgl_basic'}).findAll('tr')[-1]
    # what if there is no 'latest_row'

    boxscore = { field : cast(latest_row.find('td', {'data-stat' : field }).text) \
        for (field, cast) in target_fields }

    return boxscore

def get_latest_stats(player_id):
    url = player_url(player_id)
    page = requests.get(url)
    stats = extract_stats(page)
    return stats

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
    logger = logging.getLogger(logger_name(__file__))

    logger.info('START')

    for line in sys.stdin:
        player = json.loads(line)
        logger.debug(f'IN player[{player}]')

        try:
            stats = get_latest_stats(player['player_id'])
            player.update(stats)

            print(json.dumps(player), flush=True)
            logger.debug(f'OUT player[{player}]')

        except AttributeError:
            logger.warn(f'Did not find latest stats for player[{player}]')
            pass

    logger.info('STOP')
