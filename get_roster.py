#!/usr/bin/env python

from oauth_tools.oauth_client import make_default_oauth_client
from log_utils import logger_name

import logging
import json
import sys

PLAYER_NAME = 2
TEAM_NAME = 5
TEAM_NAME_ALT = 6

def get_roster(account_id, team_key):
    cli = make_default_oauth_client(account_id)

    url = f'https://fantasysports.yahooapis.com/fantasy/v2/team/{team_key}/roster/players?format=json'
    rsp = cli.get(url)
    if rsp.ok:
        r = json.loads(rsp.content)
        for key, record in r['fantasy_content']['team'][1]['roster']['0']['players'].items():
            if key == 'count': continue
            player = record['player'][0]

            try:
                full_name = player[PLAYER_NAME]['name']['full']

                try:
                    team_name = player[TEAM_NAME]['editorial_team_full_name']
                except KeyError:
                    team_name = player[TEAM_NAME_ALT]['editorial_team_full_name']

                yield { 'name': full_name, 'team': team_name }

            except Exception as e:
                # print(f'exception for player[{player}]')
                continue

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
    logger = logging.getLogger(logger_name(__file__))

    logger.info('START')

    for line in sys.stdin:
        account = json.loads(line)
        logger.debug(f'IN account[{account}]')

        for player in get_roster(account['account_id'], account['team_key']):
            print(json.dumps(player))
            logger.debug(f'OUT player[{player}]')

    logger.info('STOP')
