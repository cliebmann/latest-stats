import json
import sys

class InteractContinue(ValueError): pass

# func just has to be callable
def interact(func, myIn=sys.stdin, myOut=sys.stdout, multi_output=False):
    def send_output(obj):
        print(json.dumps(obj), file=myOut, flush=True)

    for line in myIn:
        input = json.loads(line)
        try:
            output = func(input)
            if multi_output:
                for o in output:
                    send_output(o)
            else:
                send_output(output)
        except InteractContinue:
            continue
