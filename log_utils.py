import logging
import os

def logger_name(fname):
    return f'{os.path.basename(os.path.dirname(fname))}.{os.path.splitext(os.path.basename(fname))[0]}'
