
# not ideal but ok
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from log_utils import logger_name

from .redirect_server import ClientRedirectServer, ClientRedirectHandler

from requests_oauthlib import OAuth2Session
import webbrowser

import logging
import os

SCOPES = ['fspt-w']
AUTH_URI = 'https://api.login.yahoo.com/oauth2/request_auth'
TOKEN_URI = 'https://api.login.yahoo.com/oauth2/get_token'
REFRESH_URI = 'https://api.login.yahoo.com/oauth2/get_token'


# macOS - forward requests from port 8080/5000 to port 80
# http://blog.brianjohn.com/forwarding-ports-in-os-x-el-capitan.html
# $ cat /etc/pf.anchors/oauth_quickstart
# rdr pass on lo0 inet proto tcp from any to any port 80 -> 127.0.0.1 port 8080
# $ sudo pfctl -vnf /etc/pf.anchors/oauth_quickstart
# $ cat /etc/pf-oauth_quickstart.conf
# rdr-anchor "forwarding"
# load anchor "forwarding" from "/etc/pf.anchors/oauth_quickstart"
# $ sudo pfctl -ef /etc/pf-oauth_quickstart.conf
# $ sudo pfctl -df /etc/pf-oauth_quickstart.conf

class BaseOAuthHandshakePolicy(object):
    @staticmethod
    def needs_refresh_token(token):
        return 'refresh_token' not in token

    # returns auth_url, state
    def get_auth_url(client_id, redirect_uri):
        oauth = OAuth2Session(client_id, redirect_uri=redirect_uri, scope=SCOPES)

        auth_url, state = oauth.authorization_url(AUTH_URI)
        return auth_url, state

    def get_refresh_token(client_id, client_secret, redirect_uri, callback_path):
        logger = logging.getLogger(logger_name(__file__))

        oauth = OAuth2Session(client_id, redirect_uri=redirect_uri, scope=SCOPES)
        auth_rsp = f'{redirect_uri}{callback_path}'

        logger.debug(f'auth_rsp: {auth_rsp}')

        token = oauth.fetch_token(TOKEN_URI,
                        authorization_response=auth_rsp,
                        client_secret=client_secret)
        return token

    def make_client(client_id, client_secret, token, token_gateway):
        extra = {
            'client_id': client_id,
            'client_secret': client_secret, }

        oauth_client = OAuth2Session(client_id, token=token, auto_refresh_url=REFRESH_URI,
                            auto_refresh_kwargs=extra, token_updater=token_gateway.save)
        return oauth_client

class CLIOAuthHandshakePolicy(BaseOAuthHandshakePolicy):
    PROXY_REDIRECT_PORT = os.environ.get('PORT', 80) # (eg use ncat to forward port 80 to 8080)
    ACTUAL_REDIRECT_PORT = os.environ.get('PORT', 80) # 8080
    REDIRECT_DOMAIN = '127.0.0.1'
    REDIRECT_URI = f'http://{REDIRECT_DOMAIN}:{PROXY_REDIRECT_PORT}'

    def get_auth_url(client_id):
        return BaseOAuthHandshakePolicy.get_auth_url(client_id, __class__.REDIRECT_URI)

    def get_refresh_token(client_id, client_secret):
        logger = logging.getLogger(logger_name(__file__))

        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

        logger.debug('Setup ClientRedirectServer')
        httpd = ClientRedirectServer((__class__.REDIRECT_DOMAIN, __class__.ACTUAL_REDIRECT_PORT), ClientRedirectHandler)

        auth_url, state = __class__.get_auth_url(client_id)

        logger.debug('Open web browser')
        webbrowser.open(auth_url, new=1, autoraise=True)

        # callback fence

        logger.debug('Handle request')
        httpd.handle_request()
        query_params = httpd.query_params
        httpd.server_close()

        logger.debug(query_params)

        if query_params['state'][0] != state:
            raise RuntimeError("cli_get_refresh_token: Unexpected state in callback")

        return BaseOAuthHandshakePolicy.get_refresh_token(client_id, client_secret, \
            __class__.REDIRECT_URI, query_params['callback_url'])

# assumes server is already running on (or forwarded from) port 80
class FlaskOAuthHandshakePolicy(BaseOAuthHandshakePolicy):
    REDIRECT_PORT = os.environ.get('PORT', 80)
    REDIRECT_DOMAIN = os.environ['URL_DOMAIN']
    REDIRECT_URI = f'http://{REDIRECT_DOMAIN}:{REDIRECT_PORT}/oauth_callback'

    def get_auth_url(client_id):
        return BaseOAuthHandshakePolicy.get_auth_url(client_id, __class__.REDIRECT_URI)

    def get_refresh_token(client_id, client_secret, callback_path):
        return BaseOAuthHandshakePolicy.get_refresh_token(client_id, client_secret, \
            __class__.REDIRECT_URI, callback_path)
