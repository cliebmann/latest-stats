from .token_gateway import FileTokenGateway
from .handshake_policy import CLIOAuthHandshakePolicy

import os

from requests_oauthlib import OAuth2Session

CONSUMER_KEY = os.environ['YAHOO_CONSUMER_KEY']
CONSUMER_SECRET = os.environ['YAHOO_CONSUMER_SECRET']

def make_default_oauth_client(account_id):
    token_gateway = FileTokenGateway(account_id)

    token = token_gateway.get()
    if CLIOAuthHandshakePolicy.needs_refresh_token(token):
        token = CLIOAuthHandshakePolicy.get_refresh_token(CONSUMER_KEY, CONSUMER_SECRET)
        token_gateway.save(token)

    return CLIOAuthHandshakePolicy.make_client(CONSUMER_KEY, CONSUMER_SECRET, token, token_gateway)
