import webbrowser
from http.server import BaseHTTPRequestHandler, HTTPServer, HTTPStatus
import urllib

# from oauth2client.tools
class ClientRedirectServer(HTTPServer):
    """A server to handle OAuth 2.0 redirects back to localhost.

    Waits for a single request and parses the query parameters
    into query_params and then stops serving.
    """
    query_params = {}

# from oauth2client.tools
class ClientRedirectHandler(BaseHTTPRequestHandler):
    """A handler for OAuth 2.0 redirects back to localhost.

    Waits for a single request and parses the query parameters
    into the servers query_params and then stops serving.
    """

    def do_GET(self):
        """Handle a GET request.

        Parses the query parameters and prints a message
        if the flow has completed. Note that we can't detect
        if an error occurred.
        """
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

        self.server.query_params['callback_url'] = self.path

        parts = urllib.parse.urlparse(self.path)
        self.server.query_params.update(urllib.parse.parse_qs(parts.query))

        # example of the opposite; although it seems to add characters
        # urllib.parse.urlencode(query_params)

        self.wfile.write(b'<html><head><title>Authentication Status</title></head>')
        self.wfile.write(b'<body><p>The authentication flow has completed.</p>')
        self.wfile.write(b'</body></html>')

    def log_message(self, format, *args):
        """Do not log messages to stdout while running as cmd. line program."""
