import os
import json

APP_DIR = os.environ['APP_DIR']

DEFAULT_TOKEN = {
    'token_type': 'bearer',
    'expires_in': 3600, }

class FileTokenGateway(object):
    def credentials_path(account_id):
        credential_dir = os.path.join(APP_DIR, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        return os.path.join(credential_dir, f'{account_id}.jsn')

    def __init__(self, account_id):
        self.cred_path = FileTokenGateway.credentials_path(account_id)

    def get(self, default_token=None):
        default_token = DEFAULT_TOKEN if not default_token else default_token

        try:
            with open(self.cred_path, 'r') as f:
                return json.load(f) # token
        except FileNotFoundError:
            return default_token

    def save(self, token):
        with open(self.cred_path, 'w') as f:
            json.dump(token, f)

# not thread safe
class LocalTokenGateway(object):
    TokenMap = {}

    def __init__(self, account_id):
        self.account_id = account_id

    def get(self, default_token=None):
        default_token = DEFAULT_TOKEN if not default_token else default_token

        try:
            return LocalTokenGateway.TokenMap[self.account_id]
        except KeyError:
            return default_token

    def save(self, token):
        LocalTokenGateway.TokenMap[self.account_id] = token
