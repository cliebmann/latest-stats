#!/usr/bin/env python

from log_utils import logger_name

import subprocess
import logging
from datetime import timedelta
import time
import sys

def make_eval(args):
    def _eval():
        return subprocess.run(args)
    return _eval

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
    logger = logging.getLogger(logger_name(__file__))

    logger.info('START')

    if len(sys.argv) < 3:
        sys.exit(1)

    delay = timedelta(minutes=float(sys.argv[1])).seconds
    args = sys.argv[2:]

    f = make_eval(args)

    logger.debug(f'Running command \'{" ".join(args)}\'')

    while f():
        logger.debug('Executed command')
        time.sleep(delay)

    logger.info('STOP')
