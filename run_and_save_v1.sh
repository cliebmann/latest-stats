#!/usr/bin/env bash

ENV_FILE="/path/to/evn/file"
source $ENV_FILE

RUN_TIME=$(date +"%Y%m%d_%H%M%S")

RUN_DIR=runs/v1/$RUN_TIME
mkdir -p $RUN_DIR
cd $RUN_DIR

LOGFILE=run_and_save_v1.log
touch $LOGFILE

{ echo $(cat) | tee account.jsn \
  | get_roster.py | tee roster.jsn \
  | find_player_id.py | tee player_ids.jsn \
  | get_latest_stats.py | tee latest_stats.jsn \
  | send_twilio_sms.py | tee twilio_sids.jsn \
; } 2> $LOGFILE

exit $?
