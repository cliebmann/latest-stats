#!/usr/bin/env bash

ENV_FILE="/path/to/evn/file"
source $ENV_FILE

RUN_TIME=$(date +"%Y%m%d_%H%M%S")

RUN_DIR=runs/$RUN_TIME
mkdir -p $RUN_DIR
cd $RUN_DIR

LOGFILE=run_and_save_v2.log
touch $LOGFILE

{ cat | tee account.jsn \
  | yahoo_latest_stats.py | tee latest_stats.jsn \
  | yahoo_extract_fields.py | tee extract_fields.jsn \
  | send_twilio_sms.py | tee twilio_sids.jsn \
} 2> $LOGFILE

exit $?
