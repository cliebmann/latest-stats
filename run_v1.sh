#!/usr/bin/env bash

ENV_FILE="/path/to/env/file"
source $ENV_FILE

RUN_TIME=$(date +"%Y%m%d_%H%M%S")

LOGDIR=logs
mkdir -p $LOGDIR

LOGFILE=$LOGDIR/run.$RUN_TIME.log
touch $LOGFILE

{ echo $(cat) \
  | $APP_DIR/get_roster.py \
  | $APP_DIR/find_player_id.py \
  | $APP_DIR/get_latest_stats.py \
  | $APP_DIR/send_twilio_sms.py \
} 2> $LOGFILE

exit $?
