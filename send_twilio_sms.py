#!/usr/bin/env python

from interact import interact, InteractContinue
from log_utils import logger_name

from twilio.rest import Client
import logging
import json
import sys
import os

def convert_to_body(stats):
    fields = [f'{key}: {value}' for key, value in stats.items()]
    body = '\n'.join(fields)
    return body

def send_twilio_sms(client, body, to, from_):
    message = client.messages.create(\
        to=to,
        from_=from_,
        body=body)

    return message

class SMSMessager(object):
    def __init__(self):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
        self.logger = logging.getLogger(logger_name(__file__))

        self.logger.info('START')

        try:
            self.client = Client(os.environ['TWILIO_ACCOUNT_SID'], os.environ['TWILIO_AUTH_TOKEN'])
        except:
            self.client = None
            self.logger.error(f'Did not create twilio rest client. No requests will be processed')

        self.to = os.environ['TWILIO_TO']
        self.from_ = os.environ['TWILIO_FROM']

    def __del__(self):
        self.logger.info('STOP')

    def __call__(self, data):
        self.logger.debug(f'IN data[{data}]')

        if self.client:
            body = convert_to_body(data)
            message = send_twilio_sms(self.client, body, self.to, self.from_)

            data['twilio-msg-sid'] = message.sid

            self.logger.debug(f'OUT data[{data}]')
            return data
        else:
            raise InteractContinue

if __name__ == '__main__':
    messager = SMSMessager()
    interact(messager)
