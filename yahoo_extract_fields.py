#!/usr/bin/env python

from log_utils import logger_name
from interact import interact

import logging
import json
import sys

def extract_fields(player):
    fields = {}

    fields.update({ k: v for k, v in player.items()
        if k in set(['name', 'team', 'date']) })

    fields.update({ k: v for k, v in player['stats'].items()
        if k in set(['fgm/a', 'ftm/a', '3ptm', 'pts', 'reb', 'ast', 'stl', 'blk', 'tov']) })

    return fields

class FieldExtractor(object):
    def __init__(self):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
        self.logger = logging.getLogger(logger_name(__file__))
        self.logger.info('START')

    def __del__(self):
        self.logger.info('STOP')

    def __call__(self, player):
        self.logger.debug(f'IN player[{player}]')
        fields = extract_fields(player)
        self.logger.debug(f'OUT fields[{fields}]')
        return fields


if __name__ == '__main__':
    fe = FieldExtractor()
    interact(fe)
