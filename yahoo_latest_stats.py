#!/usr/bin/env python

from oauth_tools.oauth_client import make_default_oauth_client
from log_utils import logger_name
from interact import interact

import logging
from datetime import datetime
import json
import sys

PLAYER_INFO = 0
PLAYER_KEY = 0
PLAYER_NAME = 2
TEAM_NAME = 5
TEAM_NAME_ALT = 6

STATS = 3

def extract_stats(stats):
    def get_stat(i, stats):
        return stats[i]['stat']['value']

    return {
        'fgm/a': get_stat(0, stats),
        'fgpct': get_stat(1, stats),
        'ftm/a': get_stat(2, stats),
        'ftpct': get_stat(3, stats),
        '3ptm': get_stat(4, stats),
        'pts': get_stat(5, stats),
        'reb': get_stat(6, stats),
        'ast': get_stat(7, stats),
        'stl': get_stat(8, stats),
        'blk': get_stat(9, stats),
        'tov': get_stat(10, stats),
    }

def get_roster(oauth_client, date, team_key):
    logger = logging.getLogger(logger_name(__file__))

    url = f'https://fantasysports.yahooapis.com/fantasy/v2/team/{team_key}/roster/players/stats;type=date;date={date}?format=json'

    rsp = oauth_client.get(url)
    if rsp.ok:
        r = json.loads(rsp.content)
        for key, record in r['fantasy_content']['team'][1]['roster']['0']['players'].items():
            if key == 'count': continue

            player = record['player'][PLAYER_INFO]
            stats = extract_stats(record['player'][STATS]['player_stats']['stats'])

            if stats['pts'] == '-':
                logger.debug(f'No stats for player[{player}]')
                continue

            try:
                player_key = player[PLAYER_KEY]['player_key']
                full_name = player[PLAYER_NAME]['name']['full']
                try:
                    team_name = player[TEAM_NAME]['editorial_team_full_name']
                except KeyError:
                    team_name = player[TEAM_NAME_ALT]['editorial_team_full_name']

                yield { 'name': full_name, 'team': team_name,
                        'player_key': player_key, 'date': date,
                        'stats': stats }

            except Exception as e:
                logger.warn('Exception for player[{full_name}]')
                continue

class YahooLatestStatsGateway(object):
    def __init__(self, oauth_client=None, date=None):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s  - %(message)s')
        self.logger = logging.getLogger(logger_name(__file__))
        self.logger.info('START')
        self.oauth_client = oauth_client
        self.date = date

    @classmethod
    def Default(cls, date=None):
        return cls(date=date)

    @classmethod
    def WithOAuthClient(cls, oauth_client, date=None):
        return cls(oauth_client=oauth_client, date=date)

    def __del__(self):
        self.logger.info('STOP')

    def __call__(self, account):
        self.logger.debug(f'IN account[{account}]')

        if self.oauth_client:
            oauth_client = self.oauth_client
        else:
            oauth_client = make_default_oauth_client(account['account_id'])

        dt = self.date if self.date else datetime.today().strftime('%Y-%m-%d')

        for player in get_roster(oauth_client, dt, account['team_key']):
            self.logger.debug(f'OUT player[{player}]')
            yield player

if __name__ == '__main__':
    date = sys.argv[1] if len(sys.argv) == 2 else None
    gateway = YahooLatestStatsGateway.Default(date)
    interact(gateway, multi_output=True)
