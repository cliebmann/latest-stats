from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage
from oauth2client import tools

from auth_tools import FileTokenGateway

import argparse
import os

APP_DIR = os.environ['APP_DIR']
CONSUMER_KEY = os.environ['YAHOO_CONSUMER_KEY']
CONSUMER_SECRET = os.environ['YAHOO_CONSUMER_SECRET']

AUTH_URI = 'https://api.login.yahoo.com/oauth2/request_auth'
TOKEN_URI = 'https://api.login.yahoo.com/oauth2/get_token'
SCOPES = ['fspt-w']
REDIRECT_URI = 'https://localhost:80'

flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
# flags.noauth_local_webserver = True

def get_credentials():
    store = Storage(FileTokenGateway.credentials_path('yahoo_quickstart.jsn'))
    creds = store.get()
    if not creds or creds.invalid:
        flow = OAuth2WebServerFlow(client_id=CONSUMER_KEY, client_secret=CONSUMER_SECRET, \
                    scope=SCOPES, auth_uri=AUTH_URI, redirect_uri=REDIRECT_URI)
        creds = tools.run_flow(flow, store, flags)
        # store.put(creds)
    return creds

# Implicit Grant Flow in Oauth 2.0 is the same as Implicit Flow in OpenID Connect
# The Access Token is received directly from the authorization endpoint
# Exposes Access Token to the user and applications with access to User Agent
# Useful for authentication and short-term access to user's private data (there is no Refresh Token)
def implicit_grant_flow():
    pass

# Explicit Grant Flow in OAuth 2.0 is the same as Authorization Code Flow in OpenID Connect
# No tokens exposed to the User Agent
# Application must securely maintain Client Secret (CONSUMER_SECRET)
# Useful for long-term access to user's private data (Refresh Token can be used to refresh the Access Token)
def explicit_grant_flow():
    pass

if __name__ == '__main__':
    get_credentials()
