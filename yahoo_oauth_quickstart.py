#!/usr/bin/env python

# Run this script to both initialize a refresh token and further acquire access
# tokens. In theory, components requiring access tokens could refresh on their own.
# For Command Line Interface a separate component will be responsible for both
# acquiring an access token and associating it with a particular account. Passing
# along the access token along with account info via stdout.

from oauth_tools.oauth_client import make_default_oauth_client

import urllib

import json
import os


from urllib.parse import urlencode, urlparse, ParseResult


def get_url(cli, url):
    rsp = cli.get(url)
    # rsp.content
    # b'<?xml version="1.0" encoding="UTF-8"?>\n<error xmlns:yahoo="http://www.yahooapis.com/v1/base.rng" yahoo:lang="en-US"><description>Authentication Error.  The table fantasysports.teams.roster requires a higher security level than is provided, you provided ANY but at least USER is expected</description></error>'

    if not rsp.ok:
        print(f'Response is not ok: status_code[{rsp.status_code}] reason[{rsp.reason}]')
        return None
    else:
        return rsp

YQL_URI = 'https://query.yahooapis.com/v2/public/yql'

def get_yql(cli, yql):
    query_params = { 'q': yql, 'format': 'json' }
    parsed_url = urlparse(YQL_URI)
    encoded_query_params = urlencode(query_params)

    url = ParseResult(
            parsed_url.scheme, parsed_url.netloc, parsed_url.path,
            parsed_url.params, encoded_query_params, parsed_url.fragment
        ).geturl()

    return json.loads(get_url(cli, url).content)



if __name__ == '__main__':

    account_id = 'yahoo-quickstart'
    cli = make_default_oauth_client(account_id)

    # doesn't work:
    # resource_url = 'https://fantasysports.yahooapis.com/fantasy/v2/team/roster/375.l.55116.t.8?format=json'

    # works:
    url = 'https://fantasysports.yahooapis.com/fantasy/v2/league/375.l.55116/teams;team_key=375.l.55116.t.8?format=json'

    r = get_url(cli, url)
    print(r['fantasy_content'].keys())


    # yql = "select * from fantasysports.teams.roster where team_key='375.l.55116.t.8'"
    # r = get_yql(oauth_client, yql)
