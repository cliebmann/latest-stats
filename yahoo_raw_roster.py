#!/usr/bin/env python

from oauth_tools.oauth_client import make_default_oauth_client

from datetime import datetime
import json
import sys

account_id = "yahoo-quickstart"
team_key = "375.l.55116.t.8"

cli = make_default_oauth_client(account_id)
date = sys.argv[1] if len(sys.argv) == 2 else datetime.today().strftime('%Y-%m-%d')

# url = f'https://fantasysports.yahooapis.com/fantasy/v2/team/{team_key}/roster/players/stats?format=json'
# url = f'https://fantasysports.yahooapis.com/fantasy/v2/team/{team_key}/roster/players/stats;type=season?format=json'
# url = f'https://fantasysports.yahooapis.com/fantasy/v2/team/{team_key}/roster/players/stats;type=week;week=12?format=json'
url = f'https://fantasysports.yahooapis.com/fantasy/v2/team/{team_key}/roster/players/stats;type=date;date={date}?format=json'

rsp = cli.get(url)

if rsp.ok:
    r = json.loads(rsp.content)
    print(json.dumps(r))
