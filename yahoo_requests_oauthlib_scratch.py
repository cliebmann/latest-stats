from oauth_tools import ClientRedirectServer, ClientRedirectHandler
from oauth_tools import FileTokenGateway

from requests_oauthlib import OAuth2Session, TokenUpdated
from oauthlib.oauth2 import TokenExpiredError
import webbrowser

import json
import os


SCOPES = ['fspt-w']
AUTH_URI = 'https://api.login.yahoo.com/oauth2/request_auth'
TOKEN_URI = 'https://api.login.yahoo.com/oauth2/get_token'
REFRESH_URI = 'https://api.login.yahoo.com/oauth2/get_token'

APP_DIR = os.environ['APP_DIR']
CONSUMER_KEY = os.environ['YAHOO_CONSUMER_KEY']
CONSUMER_SECRET = os.environ['YAHOO_CONSUMER_SECRET']

REDIRECT_DOMAIN = '127.0.0.1'
PROXY_REDIRECT_PORT = 80 # (eg use ncat to forward port 80 to 8080)
ACTUAL_REDIRECT_PORT = 8080
REDIRECT_URI = f'http://{REDIRECT_DOMAIN}:{PROXY_REDIRECT_PORT}'

resource_url = 'https://fantasysports.yahooapis.com/fantasy/v2/team/roster/375.l.55116.t.8?format=json'



# http://blog.brianjohn.com/forwarding-ports-in-os-x-el-capitan.html
# $ cat /etc/pf.anchors/oauth_quickstart
# rdr pass on lo0 inet proto tcp from any to any port 80 -> 127.0.0.1 port 8080
# $ sudo pfctl -vnf /etc/pf.anchors/oauth_quickstart
# $ cat /etc/pf-oauth_quickstart.conf
# rdr-anchor "forwarding"
# load anchor "forwarding" from "/etc/pf.anchors/oauth_quickstart"
# $ sudo pfctl -ef /etc/pf-oauth_quickstart.conf
# $ sudo pfctl -df /etc/pf-oauth_quickstart.conf


# path = 'http://127.0.0.1/#access_token=CNir.HrKogNDjldiFBMFVLfKhN6kHENbkKJ0azg6C2igfVPNa8UiUNB1PW.fg1WqjIQgFaMbypxHPiEomlU3sVhxJ7aOBt0e1GchDpqw9ZEvYYq8qnh90.tpXYEI22m_vMBop5zAtDtQzpVJepwa.NLZ3OoPfsxZvjb6mC5gJSfuh..hpwPyglDa7PNaIWBbS1lqgktCXqcxfEsJ7KvzQtvXDYI1I.79enHVLUhnWzkDSvQx5xwv2YSF3b3wopvwUqdF0MwOBQr387GCttFqMq241HGsGZkcb_SVJRp7afKWmBjOkjFWLbMjYi0f76suuWPOtjfghkcAdCBdzWQXeTdC4YlFwhs9kojYGYV7Tzua_OfWpnNh0eZ6mvzE9JS_wkF9c9R1HNZ.nduzkUKmAjTnvk25F03Bq0lCYJiTi8q5EPXDYHHhCD6tBooW3zCwuLw88xu8YzhPbtyyw_KY9H1KTixClSL11bfnqp4gRPR_iYkSxKROOk_nRugmWAetjxW85SHEvptCUXWBgJH1H1DjJPcN3bVyIzvx_ueC9ijPX_Ii_q_Dm.gLqgYnKtqcrX3wbZEo6V3DLkwS7Vmp1k4uWB9khrCM6kV5LAvChN2q2LsOwA4HOF8B7CjltTBGlNWVhWh7aG3wIQpSvm4JVjip1R4fOk9RANa85UqhrBTGtWQuP0meF100uu_vKV4gKXyTnPkQcj2ydoWprZMtykOtAPFVzjImWvyI6naKNaSn2xiLXI3iQ8y.oqUaaysDCyDvew5h_tkcB69JX_f_GvxhGJ_mXCX.0Cq5AC2QGBRiOtLoE0e.kmIL_hkfK78HxVsPvhnCfteQ6f62bZxrkENNMw2UfVNDuymv9hzf1cVJUECfXqgBWmSILCA8jU62Q5yQSiA5UTRDNE.lNIwFXT.ZohXw3qhIcTzCvToMGHE2gxUUuRIq6gj5zTYH6y9blSOdQrslf5oU0ehEc9stlIhoG_2inR8E4ppa8PJDAwEkFfKtGxrTp91UB0gYfIAWW9RAxC00Y5h1A738fhMrvAB5KKCLdrx0bLwTXReQm6H4LRK3NDt7KDRL9g--&token_type=bearer&expires_in=3600&state=UKfRS4K2a2KsBdB157qJGWcIOoJBYW'


################################################################################
# Workflows
## Web Application (Authorization Grant Type Flow)
## Mobile Application (Implicit Code Grant Type Flow)
## Legacy Application (Resource Owner Password Credentials Type Flow)
## Backend Application (Resource Owner Client Credentials Grant Type Flow)
################################################################################

################################################################################
# 1. Session setup
################################################################################

################################################################################
## (Web Application) User authorization through redirectection
httpd = ClientRedirectServer((REDIRECT_DOMAIN, ACTUAL_REDIRECT_PORT), ClientRedirectHandler)

oauth = OAuth2Session(CONSUMER_KEY, redirect_uri=REDIRECT_URI, scope=SCOPES)
auth_url, state = oauth.authorization_url(AUTH_URI)
# auth_url = 'https://api.login.yahoo.com/oauth2/request_auth?response_type=code&client_id=dj0yJmk9bW5TVzd0dE9HWlVsJmQ9WVdrOVZrOUdOa0poTkhNbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0wNw--&redirect_uri=http%3A%2F%2F127.0.0.1%3A80&scope=fspt-w&state=zBGPScj9sfQz9CzzpJODvSvME542C0'

webbrowser.open(auth_url, new=1, autoraise=True)
print(f'Your browser has been opened to visit: {auth_url}')

httpd.handle_request()
print(httpd.query_params)
# {'code': ['z5n8r2e'], 'state': ['zBGPScj9sfQz9CzzpJODvSvME542C0']}

# assert httpd.query_params['state'] == state

authorization_response = httpd.query_params['callback_url']
################################################################################

################################################################################
## (Mobile Application) Get authorization url
# oauth = OAuth2Session(client=MobileApplicationClient(client_id=CONSUMER_KEY), scope=SCOPES)
# auth_url, state = oauth.authorization_url(AUTH_URI)
################################################################################

################################################################################
## (Legacy Application)
# oauth = OAuth2Session(client=LegacyApplicationClient(client_id=CONSUMER_KEY))
################################################################################

################################################################################
## (Backend Application)
# client = oauthlib.oauth2.BackendApplicationClient(client_id=CONSUMER_KEY)
# oauth = OAuth2Session(client=client)
################################################################################


################################################################################
# 2. Fetch access token
################################################################################

################################################################################
## (Web Application)
# token = oauth.fetch_token(token_url, authorization_response=authorization_response, client_secret=CONSUMER_SECRET)
################################################################################

################################################################################
## (Mobile Application)
# rsp = oauth.get(auth_url)
# token = oauth.token_from_fragment(rsp.url)
################################################################################

################################################################################
## (Legacy Application)
# token = oauth.fetch_token(token_url=token_url, username=username, password=password, client_id=CONSUMER_KEY, client_secret=CONSUMER_SECRET)
################################################################################

################################################################################
## (Backend Application)
# token = oauth.fetch_token(token_url=token_url, client_id=CONSUMER_KEY, client_secret=CONSUMER_SECRET)
################################################################################

################################################################################
# 3. Access protected resources
################################################################################

################################################################################
## (Web Application)
# r = oauth.get(resource_url)
################################################################################

################################################################################
# 4. Refresh token
## expires_in is a credential given with the access and refresh token indiciating in how many seconds from now the access token expires.
################################################################################

token = {
    'access_token': token,
    'refresh_token': 'asdfkljh23490sdf', # hmmm
    'token_type': 'Bearer',
    'expires_in': '-30', }    # initially 3600, need to be updated by you

extra = {
    'client_id': CONSUMER_KEY,
    'client_secret': CONSUMER_SECRET, }

token_gateway = FileTokenGateway('yahoo_quickstart')
token_saver = token_gateway.save

################################################################################
# 4.1 Define Try-Catch TokenExpiredError on each request
# Most basic version in which an error is raised when refresh is necessary but
# refreshing is done manually.

# try:
#     client = OAuth2Session(CONSUMER_KEY, token=token)
#     r = client.get(resource_url)
# except TokenExpiredError as e:
#     token = client.refresh_token(REFRESH_URL, **extra)
#     token_saver(token)
#
# client = OAuth2Session(CONSUMER_KEY, token=token)
# r = client.get(resource_url)
################################################################################

################################################################################
# 4.2 Define automatic token refresh but manual update
# Middle between the basic and convenient refresh methods in which a token is
# automatically refreshed, but saving the new token is done manually.

# try:
#     client = OAuth2Session(CONSUMER_KEY, token=token,
#             auto_refresh_kwargs=extra, auto_refresh_url=REFRESH_URL)
#     r = client.get(resource_url)
# except TokenUpdated as e:
#     token_saver(e.token)
################################################################################

################################################################################
# 4.3 Define automatic token refresh and update
# recommended method will automatically fetch refresh tokens and save them. It
# requires no exception catching and results in clean code. Remember however that
# you still need to update expires_in to trigger the refresh.
client = OAuth2Session(CONSUMER_KEY, token=token, auto_refresh_url=REFRESH_URL,
    auto_refresh_kwargs=extra, token_updater=token_saver)
r = client.get(resource_url)
################################################################################
